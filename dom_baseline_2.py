import turtle

def prostokat(lewy_dolny_rog_x, lewy_dolny_rog_y, wysokosc, szerokosc):
    turtle.penup()
    turtle.setposition(lewy_dolny_rog_x, lewy_dolny_rog_y)
    turtle.pendown()
    turtle.setposition(lewy_dolny_rog_x + szerokosc, lewy_dolny_rog_y)
    turtle.setposition(lewy_dolny_rog_x + szerokosc, lewy_dolny_rog_y + wysokosc)
    turtle.setposition(lewy_dolny_rog_x, lewy_dolny_rog_y + wysokosc)
    turtle.setposition(lewy_dolny_rog_x, lewy_dolny_rog_y)
    turtle.penup()
    
def drzwi(lewy_dolny_rog_domu_x, lewy_dolny_rog_domu_y, wysokosc_domu, szerokosc_domu):
    
    srodek_dolnej_krawedzi_domu = lewy_dolny_rog_domu_x + (szerokosc_domu / 2)
    wysokosc_drzwi = wysokosc_domu / 3
    szerokosc_drzwi = szerokosc_domu * 2 / 5
    lewy_dolny_rog_drzwi_x =srodek_dolnej_krawedzi_domu - (szerokosc_drzwi / 2)
    lewy_dolny_rog_drzwi_y = lewy_dolny_rog_domu_y
    prostokat(lewy_dolny_rog_drzwi_x, lewy_dolny_rog_drzwi_y, wysokosc_drzwi, szerokosc_drzwi)

    
def okna(lewy_dolny_rog_domu_x, lewy_dolny_rog_domu_y, wysokosc_domu, szerokosc_domu):
    lewa_krawedz_lewego_okna_x = lewy_dolny_rog_domu_x + (szerokosc_domu / 10) # współrzędna 'x' lewej krawędzi okien po lewej stronie
    lewa_krawedz_prawego_okna_x = lewy_dolny_rog_domu_x + (szerokosc_domu * 6 / 10) # współrzędna 'x' lewej krawędzi okien po prawej stronie
    dolna_krawedz_dolnego_okna_y = lewy_dolny_rog_domu_y + (wysokosc_domu / 2) # współrzędna 'y' dolnej krawędzi okien dolnych
    dolna_krawedz_gornego_okna_y = lewy_dolny_rog_domu_y + (wysokosc_domu * 5 / 7) # współrzędna 'y' dolnej krawędzi okien górnych

    wysokosc_okna = wysokosc_domu / 7
    szerokosc_okna = szerokosc_domu * 3 / 10

    prostokat(lewa_krawedz_lewego_okna_x, dolna_krawedz_dolnego_okna_y, wysokosc_okna, szerokosc_okna) # lewe, dolne okno
    prostokat(lewa_krawedz_prawego_okna_x, dolna_krawedz_dolnego_okna_y, wysokosc_okna, szerokosc_okna, ) # prawe, dolne okno
    prostokat(lewa_krawedz_lewego_okna_x, dolna_krawedz_gornego_okna_y, wysokosc_okna, szerokosc_okna, ) # lewe, górne okno
    prostokat(lewa_krawedz_prawego_okna_x, dolna_krawedz_gornego_okna_y, wysokosc_okna, szerokosc_okna, ) # prawe, górne okno

def daszek(lewy_dolny_rog_domu_x, lewy_dolny_rog_domu_y, wysokosc_domu, szerokosc_domu):
    pass

def dom(lewy_dolny_rog_domu_x, lewy_dolny_rog_domu_y, wysokosc_domu, szerokosc_domu):
    ## główna bryła
    prostokat(lewy_dolny_rog_domu_x, lewy_dolny_rog_domu_y, wysokosc_domu, szerokosc_domu)
    ## drzwi
    drzwi(lewy_dolny_rog_domu_x, lewy_dolny_rog_domu_y, szerokosc_domu, wysokosc_domu),
    ##okna
    okna(lewy_dolny_rog_domu_x, lewy_dolny_rog_domu_y, wysokosc_domu, szerokosc_domu)

dom(-50, -50, 200, 200)
dom(-200, -200, 100, 100)
dom(200, 200, 150, 150)

turtle.exitonclick()